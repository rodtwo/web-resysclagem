echo "building to /dist (...)"
ng build --base-href /

echo "copying to /server/public (...)"
xcopy /s /i /y %cd%\dist %cd%\server\public

echo "removing content on /dist for free space"
del /s dist\*

echo "done"
pause
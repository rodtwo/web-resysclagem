import { DicaService } from './../dica/dica.service';
import { PontodecoletaService } from './../pontodecoleta/pontodecoleta.service';
import { ResiduoService } from './../residuo/residuo.service';
import { CategoriaService } from './../categoria/categoria.service';
import { UsuarioService } from './../usuario/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Aba } from './aba.component';

@Component({
  selector: 'abas',
  templateUrl: './abas.component.html',
  styleUrls: ['./abas.component.css']
})
export class Abas implements OnInit {
  abas: Aba[] = [];

  constructor(private uService: UsuarioService,
              private catService: CategoriaService,
              private resService: ResiduoService,
              private pcoService: PontodecoletaService,
              private dicaService: DicaService) {
  }

  ngOnInit(){
    //Para cada service que queira abrir abas, tem que dar um subscribe em um EventEmmiter.
    this.uService.usuarioEditEmitter.subscribe(uAba => {
      this.selectAbaPeloNome(uAba);
    });
    this.catService.catEditEmitter.subscribe(catAba => {
      this.selectAbaPeloNome(catAba);
    });
    this.resService.resEditEmitter.subscribe(resAba => {
      this.selectAbaPeloNome(resAba);
    });
    this.dicaService.dicaEditEmitter.subscribe(dicAba => {
      this.selectAbaPeloNome(dicAba);
    });
    this.pcoService.pcoEditEmitter.subscribe(pcoAba => {
      this.selectAbaPeloNome(pcoAba);
    });
  }

  selectAbaPeloNome(abaNome:string){
    for(let aba of this.abas){
      if (aba.abaNome == abaNome){
        console.log(abaNome);
        this.selectAba(aba);
        break;
      }
    }
  }

  selectAba(aba: Aba) {
    this.abas.forEach((aba) => {
      aba.active = false;
    });
    aba.active = true;
  }

  addAba(aba: Aba) {
    if (this.abas.length === 0) {
      aba.active = true;
    }
    this.abas.push(aba);
  }

  onNotify(message:string):void {
    alert(message);
  }
}

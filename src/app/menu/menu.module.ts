import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Tabs } from './tabs.component';
import { Tab } from './tab.component';
import { Abas } from './abas.component';
import { Aba } from './aba.component';

@NgModule({
  declarations: [Tabs, Tab, Abas, Aba],
  imports: [
    CommonModule
  ],
  exports: [Tabs, Tab, Abas, Aba]
  })
export class MenuModule { }

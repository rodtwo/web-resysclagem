import { UsuarioService } from './../usuario/usuario.service';
import { LoginService } from './../login/login.service';
import { Component, Input } from '@angular/core';
import { Tab } from './tab.component';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
   
})
export class Tabs {
  tabs: Tab[] = [];

  constructor(private lService:LoginService,
              private uService:UsuarioService){
    /*
    this.uService.usuarioEditEmitter.subscribe(mudou => {
      for(let tab of this.tabs){
        if (tab.tabTitle == "editar") this.selectTab(tab);
      }
    });
    */
  }

  selectTab(tab: Tab) {
    this.tabs.forEach((tab) => {
      tab.active = false;
    });
    tab.active = true;
  }

  addTab(tab: Tab) {
    if (this.tabs.length === 0) {
      tab.active = true;
    }
    this.tabs.push(tab);
  }

  onNotify(message:string):void {
    alert(message);
  }

  logout(){
    this.lService.logout();
  }

}
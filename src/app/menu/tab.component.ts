import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Tabs } from './tabs.component';

@Component({
  selector: 'tab',
  templateUrl: './tab.component.html'
})
export class Tab {
  
  active = false;
  
  @Input() tabTitle: string;
  @Input() tabType: string;
  @Input() tabClass: string;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  onClick() {
    this.notify.emit('Click from nested component');
  }

  constructor(tabs:Tabs) {
    tabs.addTab(this);
  }
}

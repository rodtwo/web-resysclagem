
import { UsuarioService } from './usuario/usuario.service';
import { DicaService } from './dica/dica.service';
import { UploadService } from './upload/upload.service';
import { ResiduoService } from './residuo/residuo.service';
import { CategoriaService } from './categoria/categoria.service';
import { PontodecoletaService } from './pontodecoleta/pontodecoleta.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { LoginComponent } from './login/login.component';
import { PontodecoletaComponent } from './pontodecoleta/pontodecoleta.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { DicaComponent } from './dica/dica.component';
import { ResiduoComponent } from './residuo/residuo.component';
import { UsuariocComponent } from './usuario/usuarioc.component';
import { Usuario } from './model/usuario';
import { RelatorioComponent } from './relatorio/relatorio.component';
//Gráficos
import { ChartsModule } from 'ng2-charts';

//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginModule } from './login/login.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { UsuarioModule } from './usuario/usuario.module';
import { MenuModule } from './menu/menu.module';
import { ResiduoModule } from './residuo/residuo.module';
import { DicaModule } from './dica/dica.module';
import { CategoriaModule } from './categoria/categoria.module';
import { PontodecoletaModule } from './pontodecoleta/pontodecoleta.module';
import { RelatorioModule } from './relatorio/relatorio.module';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { LoggedComponent } from './logged.component';
import { PropertiesService } from './properties.service';
import { DatePipe, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { routing, routingchild } from "app/app.routing";

export function loadProp(propService: PropertiesService){
  return () => propService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    LoggedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    UsuarioModule,
    LoginModule,
    MenuModule,
    ResiduoModule,
    DicaModule,
    CategoriaModule,
    PontodecoletaModule,
    RelatorioModule,
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    ChartsModule,
    routing,
    routingchild
    //BrowserAnimationsModule,
    //ToastModule.forRoot()
  ],
  exports: [
  ],
  providers: [CookieService,
              UsuarioService,
              PontodecoletaService,
              CategoriaService,
              ResiduoService,
              UploadService,
              DicaService,
              NotificationsService,
              DatePipe,
              PropertiesService,
              [{provide: LocationStrategy, useClass: HashLocationStrategy}],  // routing http-server issue
              { provide: APP_INITIALIZER,
                useFactory: loadProp,
                deps: [PropertiesService],
                multi: true
              }],
  bootstrap: [AppComponent]
})
export class AppModule {

}


/*angular.module('tabsDemoDynamicHeight', ['ngMaterial']); */

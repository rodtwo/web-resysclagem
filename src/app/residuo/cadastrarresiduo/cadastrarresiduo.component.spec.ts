import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarresiduoComponent } from './cadastrarresiduo.component';

describe('CadastrarresiduoComponent', () => {
  let component: CadastrarresiduoComponent;
  let fixture: ComponentFixture<CadastrarresiduoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarresiduoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarresiduoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CookieService } from 'angular2-cookie/core';
import { Request } from './../../model/request';
import { Residuo } from 'app/model/residuo';
import { NotificationsService } from 'angular2-notifications';
import { MsgResponse } from './../../model/msg-response';
import { CategoriaService } from './../../categoria/categoria.service';
import { Categoria } from './../../model/categoria';
import { Observable } from 'rxjs/Observable';
import { ResiduoService } from './../residuo.service';
import { UploadService } from '../../upload/upload.service';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } 
    from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
//import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-cadastrarresiduo',
  templateUrl: './cadastrarresiduo.component.html',
  styleUrls: ['./cadastrarresiduo.component.css']
})
export class CadastrarresiduoComponent implements OnInit {
  residuoForm: FormGroup;
  public nome;
  public descricao;
  public imagemResiduo;
  public categoria;
  public fileList: FileList;

  constructor(
      public fb: FormBuilder, 
      private service: ResiduoService, 
      private categoriaService: CategoriaService,
      private uploadService: UploadService,
      private nService: NotificationsService,
      private cookieS: CookieService,
      private http: Http
      ) { }

  ngOnInit() {
    this.residuoForm = this.fb.group({
      nome: ["", Validators.required],
      descricao: ["", Validators.required],
      imagemResiduo: ["", Validators.required],
      categoria: ["", Validators.required]
    });
  }

  fileChange(event){
    this.fileList = event.target.files;
  }

  cadastrarResiduo(residuoForm){
    let residuo = residuoForm as Residuo;
    let img: File = this.fileList.item(0);
    residuo.imagemResiduo = img.name;
    let request = new Request(JSON.stringify(residuo));
    //console.log(this.categoria);
    this.service.novoResiduo(request).subscribe(
      data =>{
        console.log(data); // isso é so pra ver se o residuo ta chegando aqui certinho
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          this.nService.success("Inserido", msg.message);
          //Based on Bharat Chauhan response: http://stackoverflow.com/questions/32423348/angular2-post-uploaded-file
          if(msg.status && this.fileList.length > 0) {
            let file: File = this.fileList[0];
            //let formData:FormData = new FormData();
            //formData.append('uploadFile', file, file.name);
            //let file = this.imagemResiduo; //let file = event.srcElement.files;
            let inserido:Residuo = JSON.parse(msg.extra);
            let postData = {cod:inserido.codR, tipo:"RES", id:this.cookieS.get("id"), t:this.cookieS.get("token")}; // Put your form data variable.
            this.uploadService.postWithFile(null, postData, file).then(
              result => {
                let msg2:MsgResponse = JSON.parse(JSON.stringify(result));
                if(msg2.status){
                  //console.log(msg2.extra);
                  this.nService.success("Imagens", msg2.message);
                  // atualizaar view
                  residuo = JSON.parse(msg.extra);
                  //residuo.imagemResiduo += ('?' + new Date().getTime());  // dibra o cache
                  this.service.residuos.push(residuo);
                  this.dpsUploadFinally(residuo);
                  this.residuoForm.reset();
                } else {
                  //console.log(msg2.extra);
                  this.nService.error("Imagens", msg2.message);
                }
              }, error => {
                this.nService.error("Imagens - Erro", error);
                console.log("Imagens - erro: "+ error);
              }
            );/**/
          } else {
            this.residuoForm.reset();
          }
        } else {
          this.nService.error("Inserido", msg.message);
          return;
        }
      },
      error =>{
        this.nService.error("Inserir - Erro", error);
        console.error("Erro ao cadastrar resíduo: " + error);
        return;
      }
    );
        
  }

  dpsUploadFinally(residuo: Residuo){
    // dibra o cache
    this.service.imagemResiduoNoCache.push(
      this.service.cacheOle(residuo.imagemResiduo)
    );  
    //this.service.mudandoImg = false;
  }

}

import { Residuo } from 'app/model/residuo';
import { Request } from './../model/request';
import { Categoria } from './../model/categoria';
import { PropertiesService } from 'app/properties.service';
import { Injectable, Component, EventEmitter, Output } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ResiduoService {
  public residuos: Array<Residuo>;
  public imagemResiduoNoCache = new Array<String>();
  public resEditEmitter = new EventEmitter<string>();
  public resEmEditar = new Residuo(0, "", "", "", new Categoria(0, "",""));
  public resEmEditarIdx: number;
  //public mudandoImg = false;

  private url: string;

  constructor(private http:Http,
              private properties:PropertiesService) { 
    //this.url = properties.getProp("ws.url") + "/server/residuo";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server/residuo";
  }

  // refresh da lista de imagens em ImagemResiduoNoCache
  cacheOleAll(){
    let i = 0;
    for (let r of this.residuos){
      this.imagemResiduoNoCache[i] = this.cacheOle(r.imagemResiduo);
      i++;
    }
  }

  cacheOle(imgRes: String): String {
    return imgRes + ('?' + new Date().getTime());
  }

  getResiduo(){
    return this.http.get(this.url + '/lista').map((res: Response) => res);
  }

  getResiduosDaCategoria(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url + "/dacategoria", body, options).map((res:Response): Residuo[] => res.json());
  }

  novoResiduo(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url, body, options).map((res:Response) => res);
  }

  passarDadosEditar(res:Residuo, idx: number){
    //console.log("editar res: " + JSON.stringify(res));
    this.resEmEditar = res;
    this.resEmEditarIdx = idx;
    this.resEditEmitter.emit("edit-res");  //listener pra mudanças - passando o nome da aba a ser chamada
  }

  updateResiduo(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.put(this.url, body, options).map((res:Response) => res);
  }

  deletaResiduo(request:Request){
    let residuo:Residuo = JSON.parse(request.objeto);
    let urlParams = "?cod=" + residuo.codR
                  + "&id=" + request.id 
                  + "&t=" + request.token;
    return this.http.delete(this.url + urlParams).map((res:Response) => res.json());
  }
}

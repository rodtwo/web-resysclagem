import { Pipe, PipeTransform } from '@angular/core';
import { Residuo } from "app/model/residuo";

@Pipe({
  name: 'filtroC'
})
export class FiltroPipeC implements PipeTransform {

  transform(items: any, termo: any): any {
    //checar se o termo buscado é indeterminado
    if (termo === undefined) return items;
    // retorna o array de tabelas atualizado
    return items.filter(function(item){
      return item.nome.toLowerCase().includes(termo.toLowerCase()) || item.codC.toString().toLowerCase().includes(termo.toString().toLowerCase()) ;

    });
  
  }

}

@Pipe({
  name: 'filtroD'
})
export class FiltroPipeD implements PipeTransform {

  transform(items: any, termo: any): any {
    //checar se o termo buscado é indeterminado
    if (termo === undefined) return items;
    // retorna o array de tabelas atualizado
    return items.filter(function(item){
      return item.nome.toLowerCase().includes(termo.toLowerCase()) || item.codD.toString().toLowerCase().includes(termo.toString().toLowerCase()) ;

    });

  
  }
}

@Pipe({
  name: 'filtroP'
})
export class FiltroPipeP implements PipeTransform {

  transform(items: any, termo: any): any {
    //checar se o termo buscado é indeterminado
    if (termo === undefined) return items;
    // retorna o array de tabelas atualizado
    
    return items.filter(function(item){
    if(item instanceof Residuo === false)
      return item.nome.toLowerCase().includes(termo.toLowerCase()) || item.codPC.toString().toLowerCase().includes(termo.toString().toLowerCase()) ;
 
    });

  
  }
}

@Pipe({
  name: 'filtroR'
})
export class FiltroPipeR implements PipeTransform {

  transform(items: any, termoR: any): any {
    //checar se o termo buscado é indeterminado
    if (termoR === undefined) return items;
    // retorna o array de tabelas atualizado
    return items.filter(function(item){
      return item.nome.toLowerCase().includes(termoR.toLowerCase()) || item.codR.toString().toLowerCase().includes(termoR.toString().toLowerCase()) ;

    });

  
  }
}

@Pipe({
  name: 'filtroSubR'
})
export class FiltroPipeSubR implements PipeTransform {

  transform(items: any, termo: any): any {
    //checar se o termo buscado é indeterminado
    if (termo === undefined) return items;
    // retorna o array de tabelas atualizado
    return items.filter(function(item){
      return item.nome.toLowerCase().includes(termo.toLowerCase()) ;

    });

  
  }
}


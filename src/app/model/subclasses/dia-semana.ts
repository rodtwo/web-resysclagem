export class DiaSemana {
    constructor(
        public dia: number,
        public abertura: string, 
        public encerramento: string
    ){}
}
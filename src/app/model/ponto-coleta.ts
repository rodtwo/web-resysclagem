import { Residuo } from './residuo';
import { HoraAtendimento } from './subclasses/hora-atendimento';
import { DiaSemana } from './subclasses/dia-semana';
export class PontoColeta{
    constructor(
        public codPC: number,
        public nome: string, 
        public pathImagem: string, 
        public endereco: string, 
        public descricao: string, 
        public telefone: string, 
        public email: string,
        public horaAtendimento: HoraAtendimento,
        public residuos: Residuo[],
        public latitude,
        public longitude,
        public msgRequisicao: string,
        public pendente: boolean
    ){}
}
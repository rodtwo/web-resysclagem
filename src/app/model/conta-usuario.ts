import { Request } from './request';
import { Usuario } from './usuario';

//ContaAdmin
export class ContaUsuario{
    constructor(
        public loginExists: boolean, 
        public passwordConfirmed: boolean, 
        public request: Request
    ){}
}
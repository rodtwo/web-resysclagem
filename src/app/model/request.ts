import { CookieService } from 'angular2-cookie/core';
export class Request {
  public id: string;
  public token: string;
  private cookieS: CookieService;

  constructor(
    public objeto: string,
  ) {
    this.cookieS = new CookieService();
    this.id = this.cookieS.get("id");
    this.token = this.cookieS.get("token");
  }
  
}
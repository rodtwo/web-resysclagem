import { Http, Response } from '@angular/http';
import { PropertiesService } from './../properties.service';
import { CookieService } from 'angular2-cookie/core';
//import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {

  private url: string;

  constructor(private cookieS: CookieService,
              private http: Http,
              private properties: PropertiesService) {
    //this.url = properties.getProp("ws.url") + "/server";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server";
  }
              //private router: Router) { }

  login(id:string, token:string){
    this.cookieS.put("logado", "sim");
    this.cookieS.put("id", id);
    this.cookieS.put("token", token);
    //this.router.navigate(['./logged']);
    window.location.reload();
  }

  logout(){
    this.cookieS.remove("logado");
    this.cookieS.remove("id");
    this.cookieS.remove("token");
    //this.router.navigate(['./login']);
    window.location.reload();
  }

  isLogged(){
    let obj:string = this.cookieS.get("logado");
    return (obj !== undefined && obj !== null);
  }

  isWSLigado(){
    this.http.get(this.url).map(res => res.text()).subscribe(
      data => {
        console.log("WS ligado!");
        return true;
      }, error => {
        console.log("WS desligado: " + error);
        return false;
      },
      () => console.log('')
    );
  }


}

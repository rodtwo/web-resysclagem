import { Usuario } from './../model/usuario';
import { LoginService } from './login.service';
import { ContaUsuario } from './../model/conta-usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { UsuarioService } from './../usuario/usuario.service';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators }
        from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Response } from "@angular/http";
import {CookieService} from 'angular2-cookie/core';
import 'rxjs/add/operator/map';
import { createHash } from "crypto";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginAdminForm: FormGroup;
  private postResponse: Response;
  public user;

  constructor(public fb: FormBuilder,
              private service: UsuarioService,
              private nService: NotificationsService,
              private router: Router,
              public route: ActivatedRoute,
              private lService: LoginService) { }

  ngOnInit() {
    this.loginAdminForm = this.fb.group({
      email: ["", Validators.required],
      senha: ["", Validators.required]
    });
  }

  loginAdmin(u){
    let usuario = u as Usuario;
    // hash
    var createHash = require('sha.js')
    var sha256 = createHash('sha256');
    var hash = sha256.update(usuario.senha, 'utf8').digest('hex')
    usuario.senha = hash;
    //
    this.service.getUsuario(usuario).subscribe(
      data =>{
        let conta:ContaUsuario = JSON.parse(data.text());
        //this.user = data;

        if(conta.loginExists && conta.passwordConfirmed){
          this.nService.success(conta.request.id, "Você está logado!");
          this.lService.login(conta.request.id, conta.request.token);    // lá dentro redireciona
        } else {
          this.nService.error("Erro", "Confira sua senha!");
        }
      },
      error => {
         console.error("Erro ao logar o administrador: " + error);
         this.nService.error("Erro","Erro no login!");
       }
    );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CategoriaComponent } from './categoria.component';
import { ListarcategoriaComponent } from './listarcategoria/listarcategoria.component';
import { CadastrarcategoriaComponent } from './cadastrarcategoria/cadastrarcategoria.component';
import { EditarcategoriaComponent } from './editarcategoria/editarcategoria.component';
import { MenuModule } from '../menu/menu.module';
import { PipeGlobalModule } from "app/pipe-global/pipe-global.module";

//import { ColorPickerModule } from 'angular2-color-picker';

@NgModule({
  declarations: [
    CategoriaComponent, ListarcategoriaComponent, CadastrarcategoriaComponent, EditarcategoriaComponent
  ],
  imports: [ 
    ReactiveFormsModule, 
    FormsModule, 
    CommonModule, 
    MenuModule,
    PipeGlobalModule
    //ColorPickerModule
  ],
  exports: [
    CategoriaComponent, 
    ListarcategoriaComponent, 
    CadastrarcategoriaComponent, 
    EditarcategoriaComponent
    ]
})
export class CategoriaModule { 
  
}

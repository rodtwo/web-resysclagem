import { Request } from './../../model/request';
import { MsgResponse } from './../../model/msg-response';
import { NotificationsService } from 'angular2-notifications';
import { Observable } from 'rxjs';
import { Categoria } from './../../model/categoria';
import { CategoriaService } from './../categoria.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastrarcategoria',
  templateUrl: './cadastrarcategoria.component.html',
  styleUrls: ['./cadastrarcategoria.component.css']
})
export class CadastrarcategoriaComponent implements OnInit {
  categoriaForm: FormGroup;
  public categoria:Categoria = new Categoria(0, "", "");

  constructor(public fb: FormBuilder, 
              private categoriaService: CategoriaService,
              private nService: NotificationsService) { }

  ngOnInit() {
    this.categoriaForm = this.fb.group({
       nome: [],
       cor: []
    });
  }

  corClick(cor:string){
    //console.log(cor);
    this.categoriaForm.controls.cor.setValue(cor);
  }

  cadastrarCategoria(categoriaForm){
    //this.categoriaForm.updateValueAndValidity();
    let categoria = categoriaForm as Categoria;
    let request = new Request(JSON.stringify(categoria));
    this.categoriaService.novaCategoria(request).subscribe(
      data =>{
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          let catInserida:Categoria = JSON.parse(msg.extra);
          this.categoriaService.categorias.push(catInserida);
          this.nService.success("Categoria", msg.message);
          this.categoriaForm.reset();
        } else {
          this.nService.error("Categoria", msg.message);
        }
      },
      error=>{
        this.nService.error("Categoria", error);
        console.error("Erro ao cadastrar categoria! "+ error);
      }
    );
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarcategoriaComponent } from './cadastrarcategoria.component';

describe('CadastrarcategoriaComponent', () => {
  let component: CadastrarcategoriaComponent;
  let fixture: ComponentFixture<CadastrarcategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarcategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarcategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

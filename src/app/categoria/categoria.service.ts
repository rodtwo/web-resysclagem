import { Request } from './../model/request';
import { PropertiesService } from 'app/properties.service';
import { Categoria } from './../model/categoria';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class CategoriaService {
  public categorias = new Array<Categoria>(); // lista de categorias

  public catEditEmitter = new EventEmitter();
  public catEmEditar:Categoria = new Categoria(0, "", "");
  public catEmEditarIdx = 0;

  private url: string;

  constructor(private http:Http,
              private properties:PropertiesService) {
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server/categoria";
  }

  getCategorias(){
    return this.http.get(this.url + "/lista").map((res: Response): Categoria[] => res.json());
  }

  // usado em editar residuo
  getCategoriaDeCod(codC: number){
    for(let c of this.categorias){
      if(c.codC == codC){
        return c;
      }
    }
  }

  novaCategoria(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url, body, options).map((res:Response) => res);
  }

  passarDadosEditar(cat:Categoria, i:number){
    this.catEmEditar = cat;
    this.catEmEditarIdx = i;
    this.catEditEmitter.emit("edit-cat");  //listener pra mudanças - passando o nome da aba a ser chamada
  }

  updateCategoria(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.put(this.url, body, options).map((res:Response) => res);
  }

  deletaCategoria(request:Request){
    let categoria:Categoria = JSON.parse(request.objeto);
    let urlParams = "?cod=" + categoria.codC
                  + "&id=" + request.id
                  + "&t=" + request.token;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.delete(this.url + urlParams).map((res:Response) => res.json());
  }
}

import { MsgResponse } from './../../model/msg-response';
import { Request } from './../../model/request';
import { NotificationsService } from 'angular2-notifications';
import { Categoria } from './../../model/categoria';
import { CategoriaService } from './../categoria.service';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators }
        from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editarcategoria',
  templateUrl: './editarcategoria.component.html',
  styleUrls: ['./editarcategoria.component.css']
})
export class EditarcategoriaComponent implements OnInit {
  editarCatForm: FormGroup;

  constructor(public fb: FormBuilder,
              private cService:CategoriaService,
              private notService:NotificationsService) { }

  ngOnInit() {
    this.editarCatForm = this.fb.group({
      codC: ["", Validators.required],
      nome: ["", Validators.required],
      cor: ["", Validators.required]
    });
  }

  corClick(cor:string){
    this.editarCatForm.controls.cor.setValue(cor);
  }

  validarAntesDoRequest(cat:Categoria){
    if(cat.codC == 0){
      this.notService.warn("Validação", "Nenhum usuário foi selecionado!");
      return false;
    }
    if(cat.nome == "" || cat.cor == "") {
      this.notService.warn("Validação", "Campos vazios!");
      return false;
    }
    return true;
  }

  editarCat(categoriaForm) {
    let cat = categoriaForm as Categoria;
    if(this.validarAntesDoRequest(cat)) {
      let request = new Request(JSON.stringify(cat));
      this.cService.updateCategoria(request).subscribe(
        data=>{
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.cService.categorias.splice(this.cService.catEmEditarIdx, 1, cat);
            this.notService.success("Editado", msg.message);
          } else {
            this.notService.error("Não editado", msg.message);
          }
        },
        error => {
          this.notService.error("Erro", error);
        }
      );
    }
  }

}

import { DataLabel } from './model/data-label';
import { NotificationsService } from 'angular2-notifications';
import { RelatorioModel } from './model/relatorio-model';
import { KeyValue } from './../model/key-value';
import { Request } from './../model/request';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { PropertiesService } from './../properties.service';
import { Relatorio } from './model/relatorio';
import { Injectable } from '@angular/core';

@Injectable()
export class RelatorioService {
  //combobox de tipos de relatório
  public optSolicitar = [new KeyValue(0, "Tipo de relatório exemplo", "query", false)];
  //a opção selecionada nessa combo vem pra cá
  public selSolicitada = this.optSolicitar[0];
  
  public rData:Relatorio;
  public relatorioAtual:RelatorioModel;
  public mostrarGrafico = false;

  private url:string;
  
  constructor(private http: Http,
              private properties: PropertiesService,
              private nService:NotificationsService) { 
      //this.url = properties.getProp("ws.url") + "/server/relatorio";
      let urlKey = "ws.url";
      if(properties.getProp("prod") as Boolean){
        urlKey += ".prod";
      }
      this.url = properties.getProp(urlKey) + "/server/relatorio";

      this.rData = new Relatorio(0, "exemplo relatorio", "stmt", "DESC", "04/06/2017", "04/06/2017", false,
                                      ['2000', '2001', '2002', '2003', '2004', '2005', '2006'],
                                      [
                                        {data: [65, 59, 80, 81, 56, 55, 40], label: 'Exemplo'},
                                        {data: [28, 48, 40, 19, 86, 27, 90], label: 'Relatório'}
                                      ]
                                    )
       this.relatorioAtual = new RelatorioModel(this.rData);
       this.mostrarGrafico = true;
  }

  tiposRelatorio(){
    return this.http.get(this.url).map((res: Response) => res);
  }

  gerarRelatorio(request:Request) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url, body, options).map((res:Response) => res);
  }

}

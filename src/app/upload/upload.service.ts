import { PropertiesService } from 'app/properties.service';
import { Injectable } from "@angular/core";
//import { ActivatedRoute, Router } from '@angular/router';
import { Http, Headers, Response, Request, RequestMethod, URLSearchParams, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
//import { Constants } from './constants';
declare var $: any;

@Injectable() 
export class UploadService {
  requestUrl: string;
  responseData: any;
  handleError: any;

  constructor(/*private router: Router,*/ 
               /*, private constants: Constants*/
              private http: Http,
              private properties:PropertiesService) {     
    //this.requestUrl = properties.getProp("ws.url") + "/server/file/fileUpload";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.requestUrl = properties.getProp(urlKey) + "/server/file/fileUpload";
    
    this.http = http;
  }

  postWithFile (url: string, postData: any, file: File) {
    if(url == null){
      url = this.requestUrl;
    }
    let headers = new Headers();
    headers.set('X-Requested-With', 'XMLHttpRequest');
    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    // For multiple files
    // for (let i = 0; i < files.length; i++) {
    //     formData.append(`files[]`, files[i], files[i].name);
    // }
    if(postData !=="" && postData !== undefined && postData !==null){
      for (var property in postData) {
          if (postData.hasOwnProperty(property)) {
              formData.append(property, postData[property]);
          }
      }
    }
    var returnReponse = new Promise((resolve, reject) => {
      return this.http.post(url, formData, {
        headers: headers
      }).subscribe(
          res => {
            this.responseData = res.json();
            resolve(this.responseData);
          },
          error => {
            /*this.router.navigate(['/login']);
            reject(error);*/
            console.log(error);
          }
      );
    });
    //console.log("formdata response: " + JSON.stringify(returnReponse));
    return returnReponse;
  }

}
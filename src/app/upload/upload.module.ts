import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadService } from './upload.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [ UploadService ],
  exports: [ ],
  declarations: [],
})
export class UploadModule { }

import { Request } from './../../model/request';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { MsgResponse } from './../../model/msg-response';
import { Residuo } from './../../model/residuo';
import { Response } from '@angular/http';
import { Usuario } from './../../model/usuario';
import { UsuarioService } from './../usuario.service';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NgModule } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-listarusuario',
  templateUrl: './listarusuario.component.html',
  styleUrls: ['./listarusuario.component.css']
  //providers: [UsuarioService]
})
export class ListarusuarioComponent implements OnInit {

  constructor(private service: UsuarioService,
              private nService: NotificationsService) {
    this.service = service;
   }

  ngOnInit() {
    this.service.listarUsuarios().subscribe(
      data => {
        let res = data as Response;
        this.service.usuarios = JSON.parse(res.text());
    });
  }

  trackByFn(index, item) {
    return index;
  }

  chamarEditar(admin:Usuario, i:number){
    this.service.passarDadosEditar(admin, i);
  }

  //deletar admin
  deletaUsuario(u){
    let usuario = u as Usuario;
    let request = new Request(u.codA)
    this.service.deletaUsuario(request).subscribe(
      data => {
        let res = data as Response;
        let msg:MsgResponse = JSON.parse(res.text());
        if(msg.status){
          this.service.usuarios = this.service.usuarios.filter(item => item !== usuario); //remove da table mostrada
          this.nService.success(msg.action, msg.message);
        } else {
          this.nService.error(msg.action, msg.message);
        }
      },
      error => {
        this.nService.error(error, "Erro ao deletar!");
      }
    );
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarusuarioComponent } from './cadastrarusuario.component';

describe('CadastrarusuarioComponent', () => {
  let component: CadastrarusuarioComponent;
  let fixture: ComponentFixture<CadastrarusuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarusuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
 
import { MsgResponse } from './../../model/msg-response';
import { NotificationsService } from 'angular2-notifications';
import { Request } from './../../model/request';
import { UsuarioService } from './../usuario.service';
import { Usuario } from './../../model/usuario';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } 
        from '@angular/forms';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-editarusuario',
  templateUrl: './editarusuario.component.html',
  styleUrls: ['./editarusuario.component.css']
})

export class EditarusuarioComponent implements OnInit {
  editarAdminForm: FormGroup;
  private postResponse: Response;
  public nome;
  public email;
  public senha;
  public valor;

  constructor(public fb: FormBuilder, 
              private service: UsuarioService,
              private notService: NotificationsService) { }

  ngOnInit() {
    this.editarAdminForm = this.fb.group({
      codA: ["", Validators.required],
      nome: ["", Validators.required],
      email: ["", Validators.required],
      senha: ["", Validators.required]
    });

    //this.getUsuario();
  }

  /*
  serviceAdminChanged(usuarioForm){
    let u = usuarioForm as Usuario;
    this.editarAdminForm.patchValue({
      codA: u.codA,
      nome: u.nome,
      email: u.email,
      senha: u.senha
    });
  }
  */

  validarAntesDoRequest(admin:Usuario){
    if(admin.codA == 0){
      this.notService.warn("Validação", "Nenhum usuário foi selecionado!");
      return false;
    }
    if(admin.senha == undefined || admin.senha.length == 0) {
      this.notService.warn("Validação", "Preencha uma senha!");
      return false;
    }
    return true;
  }

  editarAdmin(usuarioForm) {
    let adm = usuarioForm as Usuario;
    if(this.validarAntesDoRequest(adm)) {
      // hash
      var createHash = require('sha.js')
      var sha256 = createHash('sha256');
      var hash = sha256.update(adm.senha, 'utf8').digest('hex')
      adm.senha = hash;
      //
      let request = new Request(JSON.stringify(adm));
      this.service.updateUsuario(request).subscribe(
        data=>{
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.service.usuarios.splice(this.service.usuarioEmEditarIdx, 1, adm);  // replace do adm na view
            this.notService.success("Editado", msg.message);
          } else {
            this.notService.error("Não editado", msg.message);
          }
        },
        error => {
          this.notService.error("Erro", error);
        }
      );
    }
  }
}


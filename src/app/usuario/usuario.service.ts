import { Request } from './../model/request';
import { PropertiesService } from 'app/properties.service';
import { Usuario } from './../model/usuario';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable, Component, EventEmitter } from '@angular/core';
import { Observable } from "rxjs/Rx";


@Injectable()
export class UsuarioService {
  public usuarios = new Array<Usuario>();

  public usuarioEditEmitter = new EventEmitter();
  public usuarioEmEditar:Usuario = new Usuario(0, "", "", "");
  public usuarioEmEditarIdx = 0;
  
  private url: string;
  private baOn: boolean;
  private ba: string;

  constructor(private http:Http,
              private properties:PropertiesService) {
    //this.url = properties.getProp("ws.url") + "/server/admin";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server/admin";

    this.baOn = properties.getProp("ws.ba.on");
    this.ba = properties.getProp("ws.ba");
  }

  listarUsuarios(){
    return this.http.get(this.url + '/lista').map((res: Response) => res);
  }

  getUsuario(usuario:Usuario){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    if(this.baOn)
      headers.append("Authorization", "Basic " + btoa(this.ba));
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(usuario);
    return this.http.post(this.url + '/session'/*+ nome + "&senha" + senha*/, body, options).map((res:Response) => res);
  }

  novoUsuario(request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url + '/cadastro', body, options).map((res:Response) => res);
  }

  passarDadosEditar(admin:Usuario, i:number){
    //console.log(i + " idx: editar usuario: " + JSON.stringify(admin));
    this.usuarioEmEditar = admin;
    this.usuarioEmEditarIdx = i;
    this.usuarioEditEmitter.emit("edit-admin");  //listener pra mudanças - passando o nome da aba a ser chamada
  }

  updateUsuario(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.put(this.url, body, options).map((res:Response) => res);
  }

  deletaUsuario(request:Request){
    let urlParams = "?cod=" + request.objeto 
                  + "&id=" + request.id 
                  + "&t=" + request.token;
    return this.http.delete(this.url + urlParams).map((res:Response) => res);
  }
}

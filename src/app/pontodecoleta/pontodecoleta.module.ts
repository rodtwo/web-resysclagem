import { PontodecoletaService } from './pontodecoleta.service';
import { ResiduoService } from './../residuo/residuo.service';
import { CategoriaService } from './../categoria/categoria.service';
import { PropertiesService } from "app/properties.service";
import { NotificationsService } from "angular2-notifications";
import { UploadService } from "app/upload/upload.service";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { PontodecoletaComponent } from './pontodecoleta.component';
import { PontodecoletapendenteComponent } from './pontodecoletapendente/pontodecoletapendente.component';
import { CadastrarpontoComponent } from './cadastrarponto/cadastrarponto.component';
import { EditarpontoComponent } from './editarponto/editarponto.component';
import { ListarpontoComponent } from './listarponto/listarponto.component';
import { MenuModule } from '../menu/menu.module';
import { AgmCoreModule } from "@agm/core";
import { PipeGlobalModule } from "app/pipe-global/pipe-global.module";

@NgModule({
  imports: [
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCEjFZd9GsmTwmR1kwNqp1uB0b18D9iTs8",
      libraries: ["places"]
    }),
    CommonModule, 
    ReactiveFormsModule, 
    FormsModule, 
    CommonModule, 
    MenuModule,
    PipeGlobalModule 
  ],
  declarations: [
    PontodecoletapendenteComponent, 
    CadastrarpontoComponent, 
    EditarpontoComponent, 
    ListarpontoComponent, 
    PontodecoletaComponent
  ],
  exports: [
    PontodecoletapendenteComponent, 
    CadastrarpontoComponent, 
    EditarpontoComponent, 
    ListarpontoComponent, 
    PontodecoletaComponent
  ], 
  providers: []
})
export class PontodecoletaModule {

}

import { Component, OnInit } from '@angular/core';
import { PontodecoletapendenteComponent } from './pontodecoletapendente/pontodecoletapendente.component';
import { CadastrarpontoComponent } from './cadastrarponto/cadastrarponto.component';
import { EditarpontoComponent } from './editarponto/editarponto.component';
import { ListarpontoComponent } from './listarponto/listarponto.component';


@Component({
  selector: 'app-pontodecoleta',
  templateUrl: './pontodecoleta.component.html',
  styleUrls: ['./pontodecoleta.component.css']
})
export class PontodecoletaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
 

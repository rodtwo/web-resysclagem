/// <reference types="@types/googlemaps" />
import { CookieService } from 'angular2-cookie/core';
import { MsgResponse } from './../../model/msg-response';
import { DiaSemana } from './../../model/subclasses/dia-semana';
import { HoraAtendimento } from './../../model/subclasses/hora-atendimento';
import { Request } from './../../model/request';
import { PontoColeta } from './../../model/ponto-coleta';
import { Residuo } from './../../model/residuo';
import { NotificationsService } from 'angular2-notifications';
import { PropertiesService } from './../../properties.service';
import { UploadService } from './../../upload/upload.service';
import { ResiduoService } from './../../residuo/residuo.service';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { PontodecoletaService } from './../pontodecoleta.service';
import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators }
        from '@angular/forms';

@Component({
  selector: 'app-editarponto',
  templateUrl: './editarponto.component.html',
  styleUrls: ['./editarponto.component.css']
})
export class EditarpontoComponent implements OnInit {
  editarPontoForm:FormGroup;
  public nome;
  public imgNomeAntes: string;
  public file: File;
  public descricao;
  public telefone;
  public email;
  public horaAbre: string[] = ["", "", "", "", "", "", ""];
  public horaFecha: string[] = ["", "", "", "", "", "", ""];
  public diaCheck: boolean[] = [false, false, false, false, false, false, false];
  //public residuos: Residuo[];
  public checkResiduos = new Array<Boolean>();

  // mapa
  public endereco = "";
  public latitude;
  public longitude;
  public default_coords = [39.8282, -98.5795];

  public searchControl: FormControl;
  public zoom: number;
  //
  private nomesSemana = ["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado"];

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(public fb: FormBuilder,
              public service:PontodecoletaService,
              private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private residuoService: ResiduoService,
              private uploadService: UploadService,
              private properties: PropertiesService,
              private cookieS: CookieService,
              private nService: NotificationsService) { }

  editarPontoSetup(event){
    if(this.service.pcoEmEditar.codPC != 0) {
      // img
      this.imgNomeAntes = this.service.pcoEmEditar.pathImagem;
      // checkboxes de resíduos
      this.checkResiduos = [];
      let checkCnt = 0;
      for(let r of this.residuoService.residuos){
        if( this.listContainsRes(this.service.pcoEmEditar.residuos, r) ){   // resíduos do PCO contém resíduo
          this.checkResiduos[checkCnt] = true;
        } else {
          this.checkResiduos[checkCnt] = false;
        }
        checkCnt++;
      }
      // horário de funcionamento
      this.horaAbre = ["", "", "", "", "", "", ""];
      this.horaFecha = ["", "", "", "", "", "", ""];
      this.diaCheck = [false, false, false, false, false, false, false];
      if(this.service.pcoEmEditar.horaAtendimento != undefined){
        for (let ds of this.service.pcoEmEditar.horaAtendimento.diasSemana){
          this.diaCheck[ds.dia - 1] = true;
          this.horaAbre[ds.dia - 1] = ds.abertura;
          this.horaFecha[ds.dia - 1] = ds.encerramento;
        }
      }
    }
  }

  ngOnInit() {

    this.editarPontoForm = this.fb.group({
      codPC: ["", Validators.required],
      nome: ["", Validators.required],
      endereco: ["", Validators.required],
      descricao: ["", Validators.required],
      telefone: ["", Validators.required],
      email: ["", Validators.required]
    });

    // mapa

    //set google maps defaults
    this.zoom = 4;
    this.latitude = this.default_coords[0];
    this.longitude = this.default_coords[1];

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            this.endereco = "";
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          //end completo
          this.endereco = place.formatted_address;
          this.searchControl.setValue(this.endereco);

          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  listContainsRes(list: Residuo[], r: Residuo){
    var achou = false;
    for(var i = 0; i < list.length; i++) {
        if(list[i] != undefined) {
          if (list[i].codR == r.codR) {
              achou = true;
              break;
          }
        }
    }
    return achou;
  }

  onChangeImage(event: EventTarget) {
      let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
      let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
      let files: FileList = target.files;
      this.file = files[0];
  }

  trackByFn(index, ponto) {
    return index;
  }

  private validatePonto(ponto: PontoColeta){
    /*
      Atributos obrigatórios: nome, endereco, telefone, resíduos, latitude e longitude. (@giu: revisar se é isso msm)
      Adaptar banco e WS pra que respeitem isso
    */
    //nome
    if(ponto.nome === ""){
      this.nService.warn("Atenção","'Nome' vazio!");
      return false;
    }
    //endereço (e coords)
    if(this.endereco === "" || this.searchControl.value != this.endereco){
      this.nService.warn("Atenção","Endereço vazio ou inválido!");
      //console.log("End: " + this.endereco + " | " + this.searchControl.value);
      return false;
    }
    //resíduos
    if(ponto.residuos.length === 0){
      this.nService.warn("Atenção","Nenhum resíduo selecionado!");
      return false;
    }
    for(let r of ponto.residuos){
      if(r == undefined){
        this.nService.warn("Atenção","Resíduo não selecionado!");
        return false;
      }
    }
    //horas - se checado, devem estar preenchidas
    for(let i of [0,1,2,3,4,5,6]) {
      if(this.diaCheck[i]) {
        if ( this.horaAbre[i] == undefined || this.horaAbre[i] == ""
          || this.horaFecha[i] == undefined || this.horaFecha[i] == "" ){
            this.nService.warn("Atenção","Corrija os horários de atendimento!");
            return false;
        }
      }
    }
    //outras vals
    return true;
  }

  efetivarPonto(pontoForm){
    let ponto = pontoForm as PontoColeta;
    ponto.pendente = false;
    this.editarPonto(ponto, true);
  }

  editarPonto(pontoForm, efetivar:boolean) {
    let ponto = pontoForm as PontoColeta;
    if(this.file != undefined){
      ponto.pathImagem = this.file.name;
    } else {
      ponto.pathImagem = this.imgNomeAntes;
    }
    ponto.latitude = this.latitude;
    ponto.longitude = this.longitude;
    ponto.endereco = this.endereco;
    ponto.horaAtendimento = new HoraAtendimento(new Array<DiaSemana>());
    for(let i of [0,1,2,3,4,5,6]) {
      if(this.diaCheck[i]) {
        ponto.horaAtendimento.diasSemana.push( new DiaSemana(i+1, this.horaAbre[i], this.horaFecha[i]) );
      }
    }
    ponto.residuos = new Array<Residuo>();
    let i = 0;
    for(let residuo of this.residuoService.residuos){
      if (this.checkResiduos[i]){
        ponto.residuos.push(residuo);
      }
      i++;
    }
    if(this.validatePonto(ponto)){
      //inserção no bd
      let request = new Request(JSON.stringify(ponto));
      let pco: PontoColeta = null;
      this.service.updatePontodeColeta(request).subscribe(
        data => {
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.nService.success("Ponto de Coleta", msg.message);
            pco = msg.extra;
            if(efetivar === false) {
                //console.log("ponto alterado:" + JSON.stringify(pco));
                // editando ponto que já estava na lista
                this.service.pontosDeColeta.splice(this.service.pcoEmEditarIdx, 1, ponto);
              } else {
                //console.log("ponto efetivado:" + JSON.stringify(pco));
                // efetivando ponto que estava em pendentes
                this.service.pontosDeColetaPendentes.splice(this.service.pcoEmEditarIdx, 1);
                this.service.pontosDeColeta.push(ponto);
              }
          } else {
            this.nService.error("Ponto de Coleta", msg.message);
          }
        },
        error => {
          this.nService.error("PCO - erro!", error);
        }
      )

      if(pco != null){
        //depois, up imagem
        let entered = false;
        let msg:MsgResponse = new MsgResponse("upload", false, "Erro interno", null);
        let postData = {cod:pco.codPC, tipo:"PCO", id:this.cookieS.get("id"), t:this.cookieS.get("token")};
        this.uploadService.postWithFile(null, postData, this.file)
          .then((response) => {
            msg = JSON.parse(JSON.stringify(response));
            if(msg.status){
              //console.log("Fileposting success: " + JSON.stringify(msg.message));
              this.nService.success("Upload", msg.message);
              this.editarPontoForm.reset();
            } else {
              this.nService.error("Upload", msg.message);
            }
          })
          .catch((error) => {
            this.nService.error("Upload", msg.message);
          });


      } else {
        this.editarPontoForm.reset();
      }

    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PontodecoletapendenteComponent } from './pontodecoletapendente.component';

describe('PontodecoletapendenteComponent', () => {
  let component: PontodecoletapendenteComponent;
  let fixture: ComponentFixture<PontodecoletapendenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PontodecoletapendenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PontodecoletapendenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

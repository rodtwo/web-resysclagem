import { Request } from './../../model/request';
import { MsgResponse } from './../../model/msg-response';
import { PontoColeta } from './../../model/ponto-coleta';
import { NotificationsService } from 'angular2-notifications';
import { PontodecoletaService } from './../pontodecoleta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pontodecoletapendente',
  templateUrl: './pontodecoletapendente.component.html',
  styleUrls: ['./pontodecoletapendente.component.css']
})
export class PontodecoletapendenteComponent implements OnInit {


  constructor(private pcoService: PontodecoletaService,
              private nService: NotificationsService) { }

  //var publica para o clique
  public pontoAtual;
  public determinaPonto = (ponto) => {
    if (this.pontoAtual === ponto) return;
    this.pontoAtual = ponto;
  }

  ngOnInit() {
    this.pcoService.getPontosdeColetaPendentes().subscribe(
      data => {
        this.pcoService.pontosDeColetaPendentes = JSON.parse(data.text());
        this.pcoService.atualizarPCOHorariosPend();
      });

  }

  trackByFn(index, ponto) {
    return ponto.codPC;
  }

  chamarEditar(pco:PontoColeta, i:number){
    this.pcoService.passarDadosEditar(pco, i);
  }

  deletaPonto(pco:PontoColeta, i){
    if(confirm("Deseja deletar o ponto "+ pco.codPC +"?")) {
      let request = new Request(JSON.stringify(pco));
      this.pcoService.deletaPontoDeColeta(request).subscribe(
        data => {
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.pcoService.pontosDeColetaPendentes.splice(i, 1);
            this.nService.success("Dica Deletada!", msg.message);
          } else {
            this.nService.error("Erro!", msg.message);
          }
        },
        error =>{
          this.nService.error("Erro!", error);
        }
      );
    }
  }


}

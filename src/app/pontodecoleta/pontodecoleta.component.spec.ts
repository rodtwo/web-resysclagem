import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PontodecoletaComponent } from './pontodecoleta.component';

describe('PontodecoletaComponent', () => {
  let component: PontodecoletaComponent;
  let fixture: ComponentFixture<PontodecoletaComponent>;
 
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PontodecoletaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PontodecoletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarpontoComponent } from './cadastrarponto.component';

describe('CadastrarpontoComponent', () => {
  let component: CadastrarpontoComponent;
  let fixture: ComponentFixture<CadastrarpontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarpontoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarpontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

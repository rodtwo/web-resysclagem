/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DicaComponent } from './dica.component';

describe('DicaComponent', () => {
  let component: DicaComponent;
  let fixture: ComponentFixture<DicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

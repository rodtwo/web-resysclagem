import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListardicaComponent } from './listardica.component';

describe('ListardicaComponent', () => {
  let component: ListardicaComponent;
  let fixture: ComponentFixture<ListardicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListardicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListardicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Categoria } from './../model/categoria';
import { Residuo } from 'app/model/residuo';
import { Request } from './../model/request';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Dica } from './../model/dica';
import { PropertiesService } from "app/properties.service";

@Injectable()
export class DicaService {
  public dicas: Dica[];

  public dicaEditEmitter = new EventEmitter();
  public dicaEmEditar = new Dica(0, "", "", false,
                                  new Residuo(0, "", "", "", new Categoria(0, "", "")),
                                  new Array<string>());
  public dicaEmEditarIdx;

  private url: string;

  constructor(private http: Http,
              private properties: PropertiesService) {
    //this.url = properties.getProp("ws.url") + "/server/dica";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server/dica";
  }

  getDicas() {
    return this.http.get(this.url + "/lista").map((res: Response) => res);
  }

  novaDica(request: Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url, body, options).map((res: Response) => res);
  }

  passarDadosEditar(dica:Dica, i:number){
    //console.log("editar dica: " + JSON.stringify(dica));
    this.dicaEmEditar = dica;
    this.dicaEmEditarIdx = i;
    this.dicaEditEmitter.emit("edit-dica");  //listener pra mudanças - passando o nome da aba a ser chamada
  }

  updateDica(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.put(this.url, body, options).map((res:Response) => res);
  }

  deletaDica(request:Request){
    let dica:Dica = JSON.parse(request.objeto);
    let urlParams = "?cod=" + dica.codD
                  + "&id=" + request.id
                  + "&t=" + request.token;
    return this.http.delete(this.url + urlParams).map((res:Response) => res);
  }

}

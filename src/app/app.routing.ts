import { Routes, RouterModule } from "@angular/router";
import { LoggedComponent } from "app/logged.component";
import { LoginComponent } from "app/login/login.component";
import { UsuariocComponent } from "app/usuario/usuarioc.component";
import { ModuleWithProviders } from "@angular/core";
import { AppComponent } from "app/app.component";
import { ResiduoComponent } from "app/residuo/residuo.component";
import { DicaComponent } from "app/dica/dica.component";
import { CategoriaComponent } from "app/categoria/categoria.component";
import { PontodecoletaComponent } from "app/pontodecoleta/pontodecoleta.component";
import { RelatorioComponent } from "app/relatorio/relatorio.component";
import { CadastrarusuarioComponent } from "app/usuario/cadastrarusuario/cadastrarusuario.component";
import { ListarusuarioComponent } from "app/usuario/listarusuario/listarusuario.component";
import { EditarusuarioComponent } from "app/usuario/editarusuario/editarusuario.component";
import { CadastrarresiduoComponent } from "app/residuo/cadastrarresiduo/cadastrarresiduo.component";
import { ListarresiduoComponent } from "app/residuo/listarresiduo/listarresiduo.component";
import { EditarresiduoComponent } from "app/residuo/editarresiduo/editarresiduo.component";
import { CadastrardicaComponent } from "app/dica/cadastrardica/cadastrardica.component";
import { ListardicaComponent } from "app/dica/listardica/listardica.component";
import { EditardicaComponent } from "app/dica/editardica/editardica.component";
import { CadastrarcategoriaComponent } from "app/categoria/cadastrarcategoria/cadastrarcategoria.component";
import { ListarcategoriaComponent } from "app/categoria/listarcategoria/listarcategoria.component";
import { EditarcategoriaComponent } from "app/categoria/editarcategoria/editarcategoria.component";
import { CadastrarpontoComponent } from "app/pontodecoleta/cadastrarponto/cadastrarponto.component";
import { ListarpontoComponent } from "app/pontodecoleta/listarponto/listarponto.component";
import { EditarpontoComponent } from "app/pontodecoleta/editarponto/editarponto.component";

const appRoutes: Routes = [
    { path: '', redirectTo: "/app", pathMatch: 'full' },
    { path: 'app', component: AppComponent },
    { path: 'login', component: LoginComponent },
    { path: 'logged', component: LoggedComponent},
    { path: 'usuarioc', component: UsuariocComponent },
    { path: 'residuo', component: ResiduoComponent },
    { path: 'dica', component: DicaComponent },
    { path: 'categoria', component: CategoriaComponent },
    { path: 'pontocoleta', component: PontodecoletaComponent },
    { path: 'relatorio', component: RelatorioComponent }

];

const appChildRoutes: Routes = [
    { path: 'cadastrarusuario', component: CadastrarusuarioComponent},
    { path: 'listarusuario', component: ListarusuarioComponent },
    { path: 'editarusuario', component: EditarusuarioComponent },
    { path: 'cadastrarresiduo', component: CadastrarresiduoComponent },
    { path: 'listarresiduo', component: ListarresiduoComponent },
    { path: 'editarresiduo', component: EditarresiduoComponent },
    { path: 'cadastrardica', component: CadastrardicaComponent },
    { path: 'listardica', component: ListardicaComponent },
    { path: 'editardica', component: EditardicaComponent },
    { path: 'cadastrarcategoria', component: CadastrarcategoriaComponent },
    { path: 'listarcategoria', component: ListarcategoriaComponent },
    { path: 'editarcategoria', component: EditarcategoriaComponent },
    { path: 'cadastrarponto', component: CadastrarpontoComponent },
    { path: 'listarponto', component: ListarpontoComponent },
    { path: 'editarponto', component: EditarpontoComponent },            
];

export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);
export const routingchild : ModuleWithProviders = RouterModule.forChild(appChildRoutes);
